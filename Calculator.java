public class Calculator{
	public static int add(int addend1, int addend2){
		int sum = addend1 + addend2;
		return sum;
	}
	public static int subtract(int minuend, int subtrahend){
		int difference = minuend - subtrahend;
		return difference;
	}
	public int multiply(int multiplicand, int multiplier){
		int product = multiplicand * multiplier;
		return product;
	}
	public double divide(double dividend, double divisor){
		double quotient = dividend / divisor;
		return quotient;
	}
}