public class MethodsTest{
	public static void main(String[] args){
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		methodTwoInputNoReturn(24, 16.05);
		System.out.println(methodNoInputReturnInt());
		System.out.println(sumSquareRoot(9, 5));
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no unput and returns nothing.");
		int x = 20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int number){
		System.out.println("Inside the method one input no return.");
		number = number - 5;
		System.out.println(number);
	}
	public static void methodTwoInputNoReturn(int num1, double num2){
		System.out.println(num1);
		System.out.println(num2);
	}
	public static int methodNoInputReturnInt(){
		int number = 5;
		return number;
	}
	public static double sumSquareRoot(int num1, int num2){
		int sum = num1 + num2;
		double squareroot = Math.sqrt(sum);
		return squareroot;
	}
}