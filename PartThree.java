import java.util.Scanner;

public class PartThree{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Input two numbers:");
		int num1 = reader.nextInt();
		int num2 = reader.nextInt();
		System.out.println("Addition: " + Calculator.add(num1, num2));
		System.out.println("Subtraction: " + Calculator.subtract(num1, num2));
		Calculator calc = new Calculator();
		System.out.println("Multiplication: " + calc.multiply(num1, num2));
		System.out.println("Division: " + calc.divide(num1, num2));
	}
}
